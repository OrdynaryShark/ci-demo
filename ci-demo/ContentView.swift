//
//  ContentView.swift
//  ci-demo
//
//  Created by Андрей Дурыманов on 03.05.2020.
//  Copyright © 2020 ADVERTECY LTD. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Feature 1!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
